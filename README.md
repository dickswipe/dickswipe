# Dickswipe

Ever show your friends a picture in your gallery, only to have them swipe
to the next photo and see the nudes of that hottie you've been texting?

Worry no more! With **dickswipe**, a swipe in any direction loads one
of the top Dicks of the Week. For people who can't show basic photo-sharing
manners and deserve a big 'ol Internet Dick.


# Build & Run

## Set up React Native

Check the React Native [getting started docs](https://facebook.github.io/react-native/docs/getting-started.html) to set up React Native.

## Get imgur API secrets

After that, you'll need to sign up for the [imgur API](https://api.imgur.com/endpoints). Create a new client app to get a client ID and a client secret.

Create `src/secrets.json` with the client ID and client secret:
```
{
	client_id: '<your client ID>',
	client_secret: '<your client secret>',
}
```

## Run

Running `react-native run-android` should suffice to load onto a device.


# Contributing

This project is not actively maintained in any way.
