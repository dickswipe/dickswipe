/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from 'react-native';
import Swiper from 'react-native-swiper';
import ImagePicker from 'react-native-image-picker';
import config from './secrets.json';

const SUBREDDIT = 'penis';
const EMERGENCY_BACKUP_CAT = 'http://thecatapi.com/api/images/get?format=src&type=jpg';

class dickswipe extends Component {

  state = {
      imageSource: null,
      leftDick: {uri: EMERGENCY_BACKUP_CAT},
      rightDick: {uri: EMERGENCY_BACKUP_CAT},
    };

  componentWillMount() {
    this.getRandomDick();
  }
  
  getTopDicks () {
    return fetch(
        'https://api.imgur.com/3/gallery/r/' + SUBREDDIT + '/top/month/',
        {
          method: 'GET',
          headers: {'Authorization': 'Client-ID ' + config.client_id},
        }
      ).then(
        (response) => response.json()
      );
  };

  getSpecificDick (item, cb) {
    fetch(
        'https://api.imgur.com/3/gallery/r/' + SUBREDDIT + '/' + item.id,
        {
          method: 'GET',
          headers: {'Authorization': 'Client-ID ' + config.client_id},
        }
      ).then(
        (response) => response.json()
      ).then(
        (responseJson) => {
          console.log(responseJson);
          cb(responseJson.data.link);
        }
      ).catch(
          (error) => {
            cb(EMERGENCY_BACKUP_CAT);
            console.error(error);
          }
      );
  };

  getRandomDick () {
    this.getTopDicks().then(
        (responseJson) => {
          let items = responseJson.data.filter( function (element) {
            return element.type === 'image/jpeg';
          });
          let leftItem  = items[Math.floor(Math.random()*items.length)];
          let rightItem = items[Math.floor(Math.random()*items.length)];

          this.getSpecificDick(leftItem, (uri => {this.setState({leftDick: {uri: uri}});}));
          this.getSpecificDick(rightItem, (uri => {this.setState({rightDick: {uri: uri}});}));
        }
      );
  };

  selectPhotoTapped () {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      takePhotoButtonTitle: null,
      noData: true,
      mediaType: 'photo',
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        var source;

        if (Platform.OS === 'android') {
          source = {uri: response.uri, isStatic: true};
        } else {
          source = {uri: response.uri.replace('file://', ''), isStatic: true};
        }

        this.setState({
          imageSource: source
        });
      }
    });
  }

  render() {
    return (
      <Swiper 
        style={styles.wrapper}
        showsButtons={false}
        loop={false}
        index={1}
        loadMinimal={false}
        loadMinimalSize={1}
        showsPagination={false}
      >
        <Image
          style={styles.stretch}
          source={this.state.leftDick}
        />
        <TouchableOpacity
          onPress={this.selectPhotoTapped.bind(this)}
          style={styles.stretch}
        >
          { this.state.imageSource === null ? <Text>Select a Photo</Text> :
            <Image
              style={styles.stretch}
              source={this.state.imageSource}
            />
          }
        </TouchableOpacity>
        <Image
          style={styles.stretch}
          source={this.state.rightDick}
        />
      </Swiper>
    );
  }
}

const styles = StyleSheet.create({
  stretch: {
    flex: 1,
    width: null,
    height: null,
  },
  wrapper: {
  },
});

AppRegistry.registerComponent('dickswipe', () => dickswipe);
